const UserModel = require('../models/users');
const ModelConstants = require('../constant/ModelConstants');



class UserDAO{
    constructor() {

    }

    findUserDetails(query){
        return UserModel.findOne(query);
    }
    updateUserDetails(user_id, updateData){
        return UserModel.findOneAndUpdate({_id: user_id}, updateData, {new:  true});
    }
    saveUserDetails(payloadData){
        const user = new UserModel(payloadData);
        return user.save();
    }

    updateUserDetailsByQuery(query, updateData){
        return UserModel.findOneAndUpdate(query, updateData, {new:  true});
    }

    updateUserDetailUpsert(user_id, updateData){
        return UserModel.findOneAndUpdate({_id: user_id}, updateData, {new:  true, upsert: false});
    }

    findAggregate(query){
        return UserModel.aggregate(query);
    }

    findAndUpdateUser(userId, userData) {
        return UserModel.findByIdAndUpdate({ _id: userId }, userData);
    }
    findUser(userId) {
        return UserModel.findById(userId)
    }
    findSellers(skip, limit){
        return UserModel.find({user_type: ModelConstants.SELLER, is_profile_completed: true})
        .sort({created_at: -1})
        .skip(skip)
        .limit(limit);
    }

    findUsers(query){
        return UserModel.find(query);
    }

    updateMany(query, updateData) {
        return UserModel.updateMany(query, updateData)
    }

    findCount(query){
        return UserModel.count(query);
    }

    updateMultiple(query, updateData){
        return UserModel.update(query, updateData, {"multi": true});
    }
    


}
module.exports = new UserDAO();