const mongoose = require('mongoose')
require('mongoose-double')(mongoose)
require('mongoose-long')(mongoose);
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const ModelConstant = require('../constant/ModelConstants');


var SchemaTypes = mongoose.Schema.Types;

var Address = new Schema({
    name     : String,
    email : String,
    phone_number : String,
    address  : String,
    address2  : String,
    address3  : String,
    city : String,
    country : String,
    state : String,
    zip_code : String,
    is_selected :{
      type : Boolean,
  	  default : false
    }
});

var Notification = new Schema({
    name     : String,
    status  : Boolean,
});

const userSchema = new mongoose.Schema({
  user_name: {
    type: String,
    trim: true,
    default: "",
  },
  password: {
    type: String,
    trim: true,
  },
  phone_number: {
    type: String,
    trim: true,
    default: "",
  },
  email: {
    type: String,
    trim: true,
    sparse: true,
    default: "",

  },
  gender: {
    type: String,
    trim: true,
    default: "",

  },
  addresses: [Address],
  language : String,
  currency : String,
  notifications : [Notification],
  current_latitude: {
    type: SchemaTypes.Double,
    default: 0
  },
  current_longitude: {
    type: SchemaTypes.Double,
    default: 0
  },
  image : {
    type: String,
    default: ""
  },
  device_type: {
  	type : String,
    enum : ['android','ios','web'],
    default: "web"
  },
  device_token : {
    type: String,
    default: ""
  },
  age : {
    type: String,
    default: ""
  },
  social_id : String,
  registration_type: {
  	type : String,
  	enum : ['facebook','googleplus']
  },
  is_login : Boolean,
  is_otp_verified : {
  	type : Boolean,
  	default : false
  },
  is_password_otp_verified :{
    type : Boolean,
    default : false
  },
  is_active : {
  	type : Boolean,
  	default : false
  },
  is_profile_completed : {
    type : Boolean,
    default : false
  },
  customer_id:{
    type: String,
    default: "",
  },
  is_stripe_user: {
    type: Boolean,
    default: false,
  },
  is_paypal_user: {
    type: Boolean,
    default: false,
  },
  reset_token : String,
  reset_token_expired_at : Date,
  account_name : String,
  account_number : SchemaTypes.Long,
  bank_name : String,
  ifsc_code : String,
  bsb_code : String,
  otp : Number,
  password_otp: Number,
  current_location: Schema.Types.ObjectId,
  product_location: Schema.Types.ObjectId,
  user_type: {
  	type : Number,
    enum : [ModelConstant.USER, ModelConstant.ADMIN, ModelConstant.SELLER],
    default: ModelConstant.USER
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

userSchema.methods.convertToJSON = function() {
  var obj = this.toObject()
  delete obj.password
  delete obj.otp
  delete obj.reset_token
  delete obj.reset_token_expired_at
  return obj
}

module.exports = mongoose.model('Users', userSchema);
