const bcrypt = require('bcrypt');
const _ = require('lodash');
const ModelConstant = require('../constant/ModelConstants');

class UserHelper {
    constructor() {

    }

    comparePassword(candidatePassword, cryptPassword) {
        return bcrypt.compare(candidatePassword, cryptPassword);
    }

    userMapper(user) {

        const addressData = user.addresses;
        const address = addressData.sort((a, b) => {
            return b.is_selected - a.is_selected
        })
        return {
            user_name: user.user_name,
            phone_number: user.phone_number,
            email: user.email,
            gender: user.gender,
            image: user.image,
            _id: user._id,
            addresses: address

        }
    }

    userMapping(userData) {
        return _.map(userData, (users) => {
            const Orders = users.Orders.length;
            const Wishlists = users.Wishlists.length;
            const order_success = _.size(users.Orders, (order) => {
                return order.order_status == ModelConstant.DELIVERED;
            })
            return {
                name: users.user_name,
                email: users.email,
                mobile: users.phone_number,
                gender: users.gender,
                address: users.addresses,
                orders: Orders,
                wishlists: Wishlists,
                order_success: order_success,
                image: users.image,
                age: users.age,
                order_details: users.Orders

            }

        })

    }

    sellerMapping(userData) {
        return _.map(userData, (users) => {
            const sellerData = users.sellerprofile[0];
            const Products = users.Products.length;
            return {
                name: users.user_name,
                email: users.email,
                mobile: users.phone_number,
                gender: sellerData.gender,
                address: users.addresses,
                product: Products,
                image: users.image,
                age: users.age,
                product_details: users.Products

            }

        })

    }



}

module.exports = new UserHelper();