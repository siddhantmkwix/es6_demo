class CustomError extends Error {
    constructor(error, errCode) {
        this.error = error;
        this.message = error.message;
        this.errorCode = errCode;
    }
}
