module.exports.SUCCESS = "Result found successfully";
module.exports.JOB_CREATE_SUCCESS = "Job create successfully";

module.exports.SIGN_IN_SUCCESS = "Sign in successfully";
module.exports.REGISTER_SUCCESS = "Register in successfully";
module.exports.OTP_VERIFIED = "OTP verified successfully";
module.exports.RE_SEND_OTP = "OTP re-send successfully";

module.exports.PROFILE_UPDATED = "Profile updated successfully";
module.exports.PASSWORD_RESET_SUCCESSFULLY = "Password reset successfully";
module.exports.OTP_SEND = "OTP sent successfully. Please verify and reset your password";
module.exports.PASSWORD_UPDATE = "Password changed successfully";
module.exports.ADDRESS_ADD = "Address added successfully";
module.exports.ADDRESS_UPDATE = "Address update successfully";
module.exports.ADDRESS_DELETE = "Address deleted successfully";
module.exports.SIGN_OUT_SUCCESSFULLY = "Sign out successfully"
module.exports.CATEGORY_FIND = "Category listing fetched successfully."
module.exports.CATEGORY_ADDED = "Category added successfully.";
module.exports.CREATE_PRODUCT = "Product created successfully.";
module.exports.UPDATE_PRODUCT = "Product updated successfully.";
module.exports.DELETE_PRODUCT = "Product deleted successfully.";
module.exports.ADD_CART = "Item added to cart successfully";
module.exports.UPDATE_CART = "Quantity update successfully";
module.exports.BUY_CART = "Product update successfully";

module.exports.REMOVE_CART = "Item removed from cart successfully.";
module.exports.CHECK_OUT_SUCCESSFULLY = "Cart checkout successfully."
module.exports.PROFILE_IMAGE_UPLOAD = "Profile image updated successfully.";
module.exports.SUBMIT_RATING = "Rating/Review submitted successfully.";
module.exports.PAYMENT_SUCCESS = "Payment done successfully.";
module.exports.ORDER_ADDED_SUCCESS = "Hooray! Your order has been received and is currently being processed. We’ll update you when it has been shipped.";

module.exports.ORDER_ADDED_SUCCESS = "Hooray! Your order has been received and is currently being processed. We’ll update you when it has been shipped.";


module.exports.WISHLIST_REMOVED = "Product removed from wishlist successfully.";
module.exports.WISHLIST_ADD = "Product added to wishlist successfully.";

module.exports.CARD_DELETE_SUCCESS = "Card delete successfully.";
module.exports.CARD_ADD_SUCCESS = "Card add successfully.";

module.exports.LOW_QUANTITY = "Low on Stock";
module.exports.OUT_OF_STOCK = "Out of Stock";
module.exports.ORDER_PLACED = "Order Placed";
module.exports.SHIPPED = " Order shipped";
module.exports.DELIVERED = "Order Delivered";

















