module.exports.SAVE_CARD = "Card save successfully";
module.exports.INVALID_USER = "In valid user name";
module.exports.INVALID_CREDENTIALS= "Invalid credentials";
module.exports.INVALID_PASSWORD = "In valid password";
module.exports.PHONE_REQUIRED = "Phone number is required.";
module.exports.OTP_NOT_VERIFIED = "Phone number already registered. Please verified Your Number";
module.exports.PROFILE_NOT_COMPLETED = "Phone number already registered. Please complete your profile";
module.exports.ALL_READY_REGISTERED = "You seem to be a Registered User. Please Sign In to continue.";
module.exports.OTP_PHONE_REQUIRED = "Phone_number / otp is required.";
module.exports.INVALID_OTP = "The OTP you have entered is Invalid";
module.exports.NAME_ERROR= "Name is required";
module.exports.PASSWORD_REQUIRED ="Password is required";
module.exports.GENDER_REQUIRED= "Gender is required";
module.exports.SOMETHING_WRONG = "Something wrong";
module.exports.PHONE_ERROR = 'Phone number is not registered.';
module.exports.TOKEN_REQUIRED = "Token is required.";
module.exports.INVALID_TOKEN = "Wrong token provided / token expired.";
module.exports.COUNTRY_CODE_ERROR = "Country code required.";
module.exports.NEW_PASSWORD_REQUIRED ="New password is required";
module.exports.IN_CORRECT_PASSWORD = "The password you entered is incorrect.";
module.exports.INVALID_LOGIN = "Your login credentials are incorrect.";
module.exports.ZIP_CODE_REQUIRED = "Zip code is required";
module.exports.LOCATION_REQUIRED = "Location id is required";
module.exports.LOCATION_TYPE_REQUIRED = "Location type is required";
module.exports.PARENT_ID_REQUIRED = "Parent id is required.";
module.exports.ATTRIBUTE_VALUE_ERROR = "Attribute must have value.";
module.exports.PARENT_ID_REQUIRED_AND_ATTRIBUTE_REQUIRED = " Parent id and attribute required.";
module.exports.PRODUCT_NOT_FOUND = "Product not found.";
module.exports.FILED_MISSING = "Some fields missing";
module.exports.NOT_SHIPPABLE = "Product is not shippable at given location";
module.exports.NOT_FOUND = "Not found!";
module.exports.BODY_NOT_FORMATED = "In-valid body formate.";
module.exports.CART_EMPTY = "Cart is empty..";
module.exports.IS_INCREASE_OR_IS_PRICE_REQUIRED = "One of flag (is_increase OR is_price_update) required .";
module.exports.CART_NOT_AVAILABLE = "The Item no longer available in cart.";
module.exports.IN_SUFFICIENT_QUANTITY = "The product is not available in sufficient quantity. Please try a lower quantity..";
module.exports.ATTRIBUTE_REQUIRED = "Attributed required.";
module.exports.SHIPPABLE_ERROR = "Shipping not available on selected postcode. Please change postcode and try again";
module.exports.DELIVERY_CHARGE_ERROR = "The delivery charge of product are updated. Please review your cart before proceeding.";
module.exports.PRICE_ERROR = "The price of product are updated. Please review your cart before proceeding.";
module.exports.STATUS_ERROR = "The product is no longer available";
module.exports.TOTAL_PRICE_ERROR = "The price of product are updated. Please review your cart before proceeding.";
module.exports.TOTAL_DELIVERY_CHARGE = "The delivery charge of product are updated. Please review your cart before proceeding."
module.exports.PRODUCT_NOT_FOUND = "Product not found!";
module.exports.PAY_MODE = "Pay Mode is missing";
module.exports.NOT_VALID_NOTIFICATION = "Not a valid notification id."
module.exports.IS_READ_REQUIRED = "Is read is required."
module.exports.INTERNAL_ERROR = "Some internal error "
module.exports.FILTER_FORMATE_ERROR = "Filter are not proper formate."

module.exports.PARAMS_REQUIRED = "Params required.";
module.exports.CART_DATA_REQUIRED = "Cart data required.";

module.exports.EMAIL_REGISTERED = "Email already Registered"
module.exports.USER_NOT_REGISTERED = "User is not Registered";

module.exports.PRODUCT_NOT_AVAILABLE = "Product not available";

module.exports.PRODUCT_OUT_OFF_STOCK = "The product is out of stock, Please try again later"

module.exports.PRODUCT_OUT_OFF_STOCK = "The product is out of stock, Please try again later"

module.exports.ERROR_SUFFICIENT_QUANTITY = "Insufficient quantity.";

module.exports.ACCOUNT_LOCKED = "Account locked. Please contact with admin.";































