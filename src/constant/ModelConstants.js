module.exports.STRIPE = "stripe";
module.exports.PAYPAL = "paypal";

module.exports.ADMIN = 0;
module.exports.USER = 1;
module.exports.SELLER = 2;
module.exports.IOS = "iso";
module.exports.ANDROID = "android";
module.exports.WEBSITE = "web";
module.exports.CURRENT_LOCATION = 3;
module.exports.PRODUCT_LOCATION = 4;
module.exports.TODAY_DEAL = 5;
module.exports.FEATURE_DEALS = 6;
module.exports.NEW_ARRIVALS = 7;
module.exports.BEST_SELLERS = 8;
module.exports.MOST_POPULAR = 9;
module.exports.ALL_PRODUCT = 10;
module.exports.DOMESTIC = 11;
module.exports.INTERNATIONAL = 12;
module.exports.FREE = 13;
module.exports.FIXED = 14;
module.exports.PENDING = 15;
module.exports.PROGRESS = 16;
module.exports.DELIVERED = 17;
module.exports.DECLINED = 18;

module.exports.ORDER_ADDED = 111;
module.exports.ORDER_SHIPPED = 112;
module.exports.ORDER_DELIVERED = 113;
module.exports.ORDER_ABANDONED_CART = 114;

module.exports.PUSH_NOTIFICATION = 200;








module.exports.PAYMENT_MODE = {
    STRIPE: "stripe",
    PAYPAL: "paypal"
};

