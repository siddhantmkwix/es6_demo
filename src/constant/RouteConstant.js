module.exports.PAGE = 1;
module.exports.LIMIT = 20;

module.exports.PRICE_LOW = "price_low";
module.exports.PRICE_HIGH = "price_high";
module.exports.POPULAR = "popular";
module.exports.NEW = "new";

