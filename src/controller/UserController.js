
const UserService = require('../services/UserService');
const response = require('../lib/response');
const MessageConstant = require('../constant/MessageConstants');




class UserController {
    constructor() {
    }


    async register(req, res) {
        UserService.signUp(req.body).then((user) => {
            return response.setResponse(res).success(user, MessageConstant.REGISTER_SUCCESS);
        }).catch((error) => {
            return response.setResponse(res).responseHandler(400, false, error.message, {});
        });
    }



}

module.exports = new UserController();
