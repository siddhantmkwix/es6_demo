
const UserService = require('../services/UserService');
const response = require('../lib/response');
const MessageConstant = require('../constant/MessageConstants');
const jwt = require('jsonwebtoken');






class AuthController {
    constructor() {
    }


    async signIn(req, res) {
        UserService.signInUser(req.body).then((user) => {
            return response.setResponse(res).success(user, MessageConstant.SIGN_IN_SUCCESS);
        }).catch((error) => {
            return response.setResponse(res).responseHandler(400, false, error.message, {});
        });
    }
}

module.exports = new AuthController();
