
class PaginationHandler{
    constructor(){

    }
    getPageCount(page, limit, total){
        const pages = Math.ceil( parseInt(total)/parseInt(limit));
        return {
            total,
            page,
            limit,
            pages
        }

    }
}
module.exports = new PaginationHandler();