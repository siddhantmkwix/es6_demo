//express/router
const mongoose = require('mongoose');
const AuthController = require('../controller/AuthController');
const UserController = require('../controller/UserController');
const userModel = mongoose.model('Users');
var passport = require('passport');
require('../lib/passport')(passport, userModel);
const passportJWT = require('passport-jwt');






const routes = (app) => {

    app.route('/api/v1/login').post(AuthController.signIn);

    app.route('/api/v1/register').post(UserController.register);
}

module.exports = routes;