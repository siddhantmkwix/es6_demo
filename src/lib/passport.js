const ModelConstants = require('../constant/ModelConstants')

module.exports = function(passport, user) {
    const jwt = require('jsonwebtoken');
    const passportJWT = require('passport-jwt');
    const extractJwt = passportJWT.ExtractJwt;

    const localStrategy = require('passport-local').Strategy;
    const jwtStrategy = require('passport-jwt').Strategy;
    var User = user;
    passport.use('user-authorization', new jwtStrategy({
        jwtFromRequest: extractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, function(jwtPayload, callback) {
        console.log('inside passport check: user');
        return user.findById(jwtPayload.id)
        .then(user => {
            const user_data = {id : user._id, is_active: user.is_active};
            return callback(null, user_data)
        })
        .catch(err => {
            return callback(err);
        })
    }));

    passport.use('seller-authorization', new jwtStrategy({
        jwtFromRequest: extractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, function(jwtPayload, callback) {
        console.log('inside passport check: seller' );
        return user.find({_id: jwtPayload.id, user_type: ModelConstants.SELLER})
        .then(user => {
            const user_data = {id : user._id, is_active: user.is_active};
            return callback(null, user_data)
        })
        .catch(err => {
            return callback(err);
        })
    }));

    passport.use('admin-authorization', new jwtStrategy({
        jwtFromRequest: extractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, function(jwtPayload, callback) {
        console.log('inside passport check:  admin');
        return user.findOne({_id: jwtPayload.id, user_type: ModelConstants.ADMIN})
        .then(user => {
            if(!user) {
                throw "Not Authorized";
            }
            console.log('INSID 1');
            const user_data = {id : user._id, is_active: user.is_active};
            return callback(null, user_data)
        })
        .catch(err => {
            console.log('INSID 2');
            return callback(err);
        })
    }));

}
