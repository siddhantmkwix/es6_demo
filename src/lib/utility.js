const crypto = require('crypto');
const jwt = require('jsonwebtoken');

require('dotenv').config();




class Utility {
    async generateToken() {
        return await new Promise((resolve, reject) => {
            crypto.randomBytes(48, function (err, buffer) {
                resolve(buffer.toString('hex'));
            });
        })
    }

    generateJwtToken(user_id) {
        var token = jwt.sign({ id: user_id },
            process.env.JWT_SECRET, {
                expiresIn: 2073600 //86400 // expires in 24 hours
            });
        return token;
    }


}

module.exports = new Utility;
