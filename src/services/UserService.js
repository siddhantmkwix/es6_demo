
const UserDao = require('../dao/UserDAO');

const ModelConstant = require('../constant/ModelConstants');
const ErrorConstants = require('../constant/ErrorConstants');
const utility = require('../lib/utility');
const UserHelper = require('../helpers/UserHelper');
const _ = require('lodash');







class UserService {

    constructor() {

    }
    async signInUser(payload) {
        const user_type = _.get(payload, "user_type", ModelConstant.USER);
        const userEmail = payload.user_name;
        const regex = new RegExp(["^", userEmail, "$"].join(""), "i");
        const query = { user_type: user_type, $or: [{ 'email': regex }, { 'phone_number': payload.user_name }] };
        const user = await UserDao.findUserDetails(query);
        if (!user) {
            throw new Error(ErrorConstants.USER_NOT_REGISTERED)
        } else if (!user.is_profile_completed) {
            throw new Error(ErrorConstants.USER_NOT_REGISTERED);
        } else if (!user.is_active) {
            throw new Error(ErrorConstants.ACCOUNT_LOCKED);
        } else {
            const comparePassword = await UserHelper.comparePassword(payload.password, user.password);
            if (!comparePassword) {
                throw new Error(ErrorConstants.INVALID_CREDENTIALS);
            }
            const token = utility.generateJwtToken(user._id);
            const device_token = (payload.device_token) ? payload.device_token : user.device_token;
            const device_type = (payload.device_type) ? payload.device_type : user.device_token;
            const is_login = true;
            const updateUser = await UserDao.updateUserDetails(user._id, { device_token, device_type, is_login });
            let rec = updateUser.convertToJSON();
            rec.token = token;
            return rec;
        }

    }



    async signUp(payload) {
        const user_type = (payload.user_type) ? payload.user_type : ModelConstant.USER;
        if (!payload.phone_number) {
            throw new Error(ErrorConstants.PHONE_REQUIRED);
        } else {
            const query = { phone_number: payload.phone_number, user_type: user_type };
            const user = await UserDao.findUserDetails(query);
            if (user) {
                if (!user.is_otp_verified) {
                    const token = utility.generateJwtToken(user._id);
                    let rec = user.convertToJSON();
                    rec.token = token
                    return rec;
                } else if (!user.is_profile_completed) {
                    const token = utility.generateJwtToken(user._id);
                    let rec = user.convertToJSON();
                    rec.token = token;
                    return rec;
                } else if (!user.is_active) {
                    throw new Error(ErrorConstants.ACCOUNT_LOCKED);
                } else {
                    throw new Error(ErrorConstants.ALL_READY_REGISTERED);
                }
            }
            const saveQuery = {
                phone_number: payload.phone_number,
                user_type,
                otp: random
            };
            const registerUser = await UserDao.saveUserDetails(saveQuery);
            return registerUser;
        }
    }


}
module.exports = new UserService();