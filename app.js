const express = require('express');        // call express
const app = express();                 // define our app using express
const bodyParser = require('body-parser');
var cors = require('cors')
require('dotenv').config();
var bb = require('express-busboy');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cors());
bb.extend(app, {
    upload: true,
    path: 'uploads',
    allowedPath: /./
});

// ROUTES FOR OUR API
// =============================================================================
const router = express.Router();


require('./src/routes/userRoute')(app);




// get an instance of the express Router

module.exports = app;
